#Inicio de la aplicación
require 'active_record'
require 'faker'
require 'colorize'
require_relative './master_orm/country.rb'
require_relative './master_orm/consignee.rb'
require_relative './master_orm/container.rb'
require_relative './master_orm/destination.rb'
require_relative './master_orm/nvocc.rb'
require_relative './master_orm/origin.rb'
require_relative './master_orm/routes_nvocc.rb'
require_relative './master_orm/shipping_company.rb'
require_relative './master_orm/bl_house.rb'
require_relative './master_orm/bl_master.rb'
require_relative './master_orm/rented_container.rb'
require_relative './master_orm/travel.rb'
require_relative './master_orm/vessel.rb'
require_relative './master_orm/route_shipping_company.rb'
require_relative 'db/connection.rb'

class Main

		#Menú principal
	def menu ()
		system("clear")
		print "                            **********BIENVENIDO AL SISTEMA DE REPORTES DE LA NAVIERA**********\n".colorize(:light_cyan)
		print "\nPOR FAVOR ELIJA UNA DE LAS SIGUIENTES OPCIONES:\n".upcase.colorize(:color => :light_cyan)
		print "\n 1- VER LA REPRESENTACIÓN DE LOS DATOS DE UN BL-MASTER EN FÍSICO ".upcase.colorize(:color => :light_cyan)
		print "                         2- ".upcase.colorize(:light_cyan)
		print "\n\n 3- ".upcase.colorize(:color => :light_cyan)
		print "                           4- VER LA CANTIDAD DE CONTAINERS POR SUS TIPOS".upcase.colorize(:color => :light_cyan)
		print "\n\n                                                    0- SALIR\n".upcase.colorize(:color => :light_cyan)
		get_single_key_main_menu
	end

		#Obtener opcion seleccionada y solicitar el reporte indicado
	def get_single_key_main_menu
		c = read_char
		case c
			when "0"
				system("clear")
				print "					CERRANDO PROGRAMA... VUELVA PRONTO\n".colorize(:color => :light_cyan)
				sleep (2)
				system("exit")
				system ("clear")
			when "1"
				system ("clear")
				print "					CARGANDO REPORTE SOLICITADO ...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				BlMaster.show_bl
			when "2"
				system ("clear")
				print "					CARGANDO REPORTE SOLICITADO...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
			
			when "3"
				system ("clear")
				print "					CARGANDO REPORTE SOLICITADO...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				
			when "4"
				system ("clear")
				print "					CARGANDO REPORTE SOLICITADO".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				show_quantity_containers_by_kind
			
			else
				system ("clear")
				print "\nSELECCIONO UNA OPCIÓN INVALIDA: #{c.inspect}\n\n ".upcase.colorize(:color => :light_cyan)
				sleep (2)
				menu()
		end

	end

	# Leer un caracter sin presionar enter y sin imprimirlo por pantalla
	def read_char
		begin
			old_state = `stty -g`
			system "stty raw -echo"
			c = STDIN.getc.chr
			if(c=="\e")
				extra_thread = Thread.new{
					c = c + STDIN.getc.chr
					c = c + STDIN.getc.chr
				}
				extra_thread.join(0.00001)
				extra_thread.kill
			end
		rescue => ex
			puts "#{ex.class}: #{ex.message}"
			puts ex.backtrace
		ensure
			system "stty #{old_state}"
		end
	return c
	end

  def show_quantity_containers_by_kind()
	print "                                             ********************CANTIDAD DE CONTENEDORES POR TIPO********************".colorize(:color => :light_cyan)
	print "\n\nCONTENEDORES TAMAÑO STANDARD (ST):".colorize(:color => :light_cyan)
	print "\n\n ST-DRY-FREIGHT: #{quantity_ST_Dry_Freight = Container.where(kind: "ST-Dry Freight").count}".colorize(:color => :light_cyan)
	print "\n\n ST-REEFER: #{quantity_ST_Dry_Freight = Container.where(kind: "ST-Reefer").count}".colorize(:color => :light_cyan)
	print "\n\n ST-OPEN TOP: #{quantity_ST_Dry_Freight = Container.where(kind: "ST-Open Top").count}".colorize(:color => :light_cyan)
	print "\n\n ST-COLLAPSABLE FLAT RACK: #{quantity_ST_Dry_Freight = Container.where(kind: "ST-Collapsable Flat Rack").count}".colorize(:color => :light_cyan)
	print "\n\nCONTENEDORES TAMAÑO HIGH CUBE (HQ):".colorize(:color => :light_cyan)
	print "\n\n HQ-REEFER: #{quantity_ST_Dry_Freight = Container.where(kind: "HQ-Reefer").count}".colorize(:color => :light_cyan)
	print "\n\n HQ-OPEN TOP: #{quantity_ST_Dry_Freight = Container.where(kind: "HQ-Open Top").count}".colorize(:color => :light_cyan)
	print "\n\n HQ-HIGH CUBE: #{quantity_ST_Dry_Freight = Container.where(kind: "HQ-High Cube").count}".colorize(:color => :light_cyan)
	print "\n\n HQ-COLLAPSABLE FLAT RACK: #{quantity_ST_Dry_Freight = Container.where(kind: "HQ-Collapsable Flat Rack").count} \n\n ".colorize(:color => :light_cyan)
	end

end
reporte = Main.new()
reporte.menu

