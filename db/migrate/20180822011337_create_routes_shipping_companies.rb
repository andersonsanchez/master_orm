
class CreateRoutesShippingCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :routes_shipping_companies do |t|
      t.integer :price_st_container, null: false
      t.integer :price_hq_container, null: false
      t.integer :timespan, null: false
      t.references :shipping_company, foreign_key: true, null: false
      t.references :origin, foreign_key: true, null: false
      t.references :destination, foreign_key: true, null: false
    end
  end
end
