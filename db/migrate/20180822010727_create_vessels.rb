class CreateVessels < ActiveRecord::Migration[5.2]
  def change
    create_table :vessels do |t|
      t.string :name, null: false
      t.integer :max_capacity, null: false
      t.references :shipping_company, foreign_key: true, null: false
    end
  end
end
