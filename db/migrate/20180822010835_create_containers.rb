class CreateContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :containers do |t|
      t.string :code, null: false
      t.integer :size, null: false
      t.string :kind, null: false
      t.references :shipping_company, foreign_key: true, null: false
    end
  end
end
