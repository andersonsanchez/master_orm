class CreateOrigins < ActiveRecord::Migration[5.2]
  def change
    create_table :origins do |t|
      t.references :country, foreign_key: true, null: false
    end
  end
end
