class CreateBlMasters < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_masters do |t|
      t.date :date, null: false
      t.references :shipping_company, foreign_key: true, null: false
      t.references :nvocc, foreign_key: true, null: false
      t.references :travel, foreign_key: true, null: false
    end
  end
end
