class CreateRoutesNvoccs < ActiveRecord::Migration[5.2]
  def change
    create_table :routes_nvoccs do |t|
      t.integer :price_st_container, null: false
      t.integer :price_hq_container, null: false
      t.integer :timespan, null: false
      t.references :nvocc, foreign_key: true, null: false
      t.references :origin, foreign_key: true, null: false
      t.references :destination, foreign_key: true, null: false
    end
  end
end
