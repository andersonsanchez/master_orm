class CreateDestinations < ActiveRecord::Migration[5.2]
  def change
    create_table :destinations do |t|
      t.references :country, foreign_key: true, null: false
    end
  end
end
