class CreateRentedContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :rented_containers do |t|
      t.references :travel, foreign_key: true, null: false
      t.references :container, foreign_key: true, null: false
    end
  end
end
