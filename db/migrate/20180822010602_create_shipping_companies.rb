class CreateShippingCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :shipping_companies do |t|
      t.string :name, null: false
      t.string :rif, null: false
    end
  end
end
