class CreateBlHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_houses do |t|
      t.date :date, null: false
      t.integer :st_container_quantity, null: false
      t.integer :hq_container_quantity, null: false
      t.references :nvocc, foreign_key: true, null: false
      t.references :consignee, foreign_key: true, null: false
      t.references :bl_master, foreign_key: true, null: false
      t.references :routes_nvocc, foreign_key: true, null: false
    end
  end
end
