class CreateConsignees < ActiveRecord::Migration[5.2]
  def change
    create_table :consignees do |t|
      t.string :name, null: false
      t.string :id_card_or_rif, null: false
    end
  end
end
