class CreateNvocc < ActiveRecord::Migration[5.2]
  def change
    create_table :nvoccs do |t|
      t.string :name, null: false
      t.string :rif, null: false
    end
  end
end
