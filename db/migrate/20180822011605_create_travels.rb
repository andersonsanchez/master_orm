class CreateTravels < ActiveRecord::Migration[5.2]
  def change
    create_table :travels do |t|
      t.date :board_date, null: false
      t.date :berth_date, null: false
      t.string :code, null: false
      t.references :routes_shipping_company, foreign_key: true, null: false
      t.references :vessel, foreign_key: true, null: false
    end
  end
end
