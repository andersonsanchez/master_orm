require_relative '../main.rb'

Dir[File.join('db', 'seeds', '*.rb')].sort.each do |seed|
  puts "Loading seed | #{File.basename(seed)}"
  load seed
end
