
#agregar un arreglo con los nombres reales de los tipos de container y llamar sample dentro de kind
#agregar un arreglo con el abecedario y lanzar sample 4 veces, para las 4 letras antes de code
Container.create!([{code: "abcd#{Faker::Number.decimal(l_digits = 7, r_digits = 0)}", size: 20, 
                    kind: 'ST-Dry Freight', shipping_company_id: ShippingCompany.all.sample.id}])

Container.create!([{code: "abcd#{Faker::Number.decimal(l_digits = 7, r_digits = 0)}", size: 40, 
                    kind: 'HQ-Reefer', shipping_company_id: ShippingCompany.all.sample.id}])                   
                    
Container.create!([{code: "abcd#{Faker::Number.decimal(l_digits = 7, r_digits = 0)}", size: 20, 
kind: 'ST-Open Top', shipping_company_id: ShippingCompany.all.sample.id}])

Container.create!([{code: "abcd#{Faker::Number.decimal(l_digits = 7, r_digits = 0)}", size: 40, 
kind: 'HQ-Collapsable Flat Rack', shipping_company_id: ShippingCompany.all.sample.id}])

Container.create!([{code: "abcd#{Faker::Number.decimal(l_digits = 7, r_digits = 0)}", size: 20, 
kind: 'ST-Collapsable Flat Rack', shipping_company_id: ShippingCompany.all.sample.id}])

Container.create!([{code: "abcd#{Faker::Number.decimal(l_digits = 7, r_digits = 0)}", size: 40, 
kind: 'HQ-Open Top', shipping_company_id: ShippingCompany.all.sample.id}])

Container.create!([{code: "abcd#{Faker::Number.decimal(l_digits = 7, r_digits = 0)}", size: 20, 
kind: 'ST-Reefer', shipping_company_id: ShippingCompany.all.sample.id}])

Container.create!([{code: "abcd#{Faker::Number.decimal(l_digits = 7, r_digits = 0)}", size: 40, 
kind: 'HQ-High Cube', shipping_company_id: ShippingCompany.all.sample.id}])






