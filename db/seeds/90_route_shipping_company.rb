10.times do |index|
    RoutesShippingCompany.create!([{price_st_container: Faker::Number.decimal(l_digits = 7),
                          price_hq_container: Faker::Number.decimal(l_digits = 3), 
                           timespan: Faker::Number.decimal(l_digits = 3), shipping_company_id: ShippingCompany.all.sample.id,
                           origin_id: Origin.all.sample.id, destination_id: Destination.all.sample.id }])
    end