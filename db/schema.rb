# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_22_012644) do

  create_table "bl_houses", force: :cascade do |t|
    t.date "date", null: false
    t.integer "st_container_quantity", null: false
    t.integer "hq_container_quantity", null: false
    t.integer "nvocc_id", null: false
    t.integer "consignee_id", null: false
    t.integer "bl_master_id", null: false
    t.integer "routes_nvocc_id", null: false
    t.index ["bl_master_id"], name: "index_bl_houses_on_bl_master_id"
    t.index ["consignee_id"], name: "index_bl_houses_on_consignee_id"
    t.index ["nvocc_id"], name: "index_bl_houses_on_nvocc_id"
    t.index ["routes_nvocc_id"], name: "index_bl_houses_on_routes_nvocc_id"
  end

  create_table "bl_masters", force: :cascade do |t|
    t.date "date", null: false
    t.integer "shipping_company_id", null: false
    t.integer "nvocc_id", null: false
    t.integer "travel_id", null: false
    t.index ["nvocc_id"], name: "index_bl_masters_on_nvocc_id"
    t.index ["shipping_company_id"], name: "index_bl_masters_on_shipping_company_id"
    t.index ["travel_id"], name: "index_bl_masters_on_travel_id"
  end

  create_table "consignees", force: :cascade do |t|
    t.string "name", null: false
    t.string "id_card_or_rif", null: false
  end

  create_table "containers", force: :cascade do |t|
    t.string "code", null: false
    t.integer "size", null: false
    t.string "kind", null: false
    t.integer "shipping_company_id", null: false
    t.index ["shipping_company_id"], name: "index_containers_on_shipping_company_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "destinations", force: :cascade do |t|
    t.integer "country_id", null: false
    t.index ["country_id"], name: "index_destinations_on_country_id"
  end

  create_table "nvoccs", force: :cascade do |t|
    t.string "name", null: false
    t.string "rif", null: false
  end

  create_table "origins", force: :cascade do |t|
    t.integer "country_id", null: false
    t.index ["country_id"], name: "index_origins_on_country_id"
  end

  create_table "rented_containers", force: :cascade do |t|
    t.integer "travel_id", null: false
    t.integer "container_id", null: false
    t.index ["container_id"], name: "index_rented_containers_on_container_id"
    t.index ["travel_id"], name: "index_rented_containers_on_travel_id"
  end

  create_table "routes_nvoccs", force: :cascade do |t|
    t.integer "price_st_container", null: false
    t.integer "price_hq_container", null: false
    t.integer "timespan", null: false
    t.integer "nvocc_id", null: false
    t.integer "origin_id", null: false
    t.integer "destination_id", null: false
    t.index ["destination_id"], name: "index_routes_nvoccs_on_destination_id"
    t.index ["nvocc_id"], name: "index_routes_nvoccs_on_nvocc_id"
    t.index ["origin_id"], name: "index_routes_nvoccs_on_origin_id"
  end

  create_table "routes_shipping_companies", force: :cascade do |t|
    t.integer "price_st_container", null: false
    t.integer "price_hq_container", null: false
    t.integer "timespan", null: false
    t.integer "shipping_company_id", null: false
    t.integer "origin_id", null: false
    t.integer "destination_id", null: false
    t.index ["destination_id"], name: "index_routes_shipping_companies_on_destination_id"
    t.index ["origin_id"], name: "index_routes_shipping_companies_on_origin_id"
    t.index ["shipping_company_id"], name: "index_routes_shipping_companies_on_shipping_company_id"
  end

  create_table "shipping_companies", force: :cascade do |t|
    t.string "name", null: false
    t.string "rif", null: false
  end

  create_table "travels", force: :cascade do |t|
    t.date "board_date", null: false
    t.date "berth_date", null: false
    t.string "code", null: false
    t.integer "routes_shipping_company_id", null: false
    t.integer "vessel_id", null: false
    t.index ["routes_shipping_company_id"], name: "index_travels_on_routes_shipping_company_id"
    t.index ["vessel_id"], name: "index_travels_on_vessel_id"
  end

  create_table "vessels", force: :cascade do |t|
    t.string "name", null: false
    t.integer "max_capacity", null: false
    t.integer "shipping_company_id", null: false
    t.index ["shipping_company_id"], name: "index_vessels_on_shipping_company_id"
  end

end
