class Destination < ActiveRecord::Base
    belongs_to :country
    belongs_to :routes_nvocc
    belongs_to :routes_shipping_company
    validates :country_id, presence: true
    #validates :countries_id, uniqueness: true
end
