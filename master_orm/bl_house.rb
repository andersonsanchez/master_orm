#validar que fecha no sea del futuro
class BlHouse < ActiveRecord::Base
    belongs_to :consignee
    has_one :consignee, dependent: :destroy
    has_one :routes_nvocc, dependent: :destroy
    belongs_to :nvocc
    belongs_to :bl_master
    has_one :nvocc, dependent: :destroy
    has_one :bl_master, dependent: :destroy
    validates :date, presence: true
    validates :st_container_quantity, presence: true
    validates :st_container_quantity, numericality: { only_integer: true }
    validates :hq_container_quantity, presence: true
    validates :hq_container_quantity, numericality: { only_integer: true }
    validates :nvocc_id, presence: true
    validates :consignee_id, presence: true
    validates :bl_master_id, presence: true
    validates :routes_nvocc_id, presence: true
end