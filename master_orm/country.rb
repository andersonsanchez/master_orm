class Country < ActiveRecord::Base

    has_one :origin, dependent: :destroy
    has_one :destination, dependent: :destroy
    validates :name, presence: true
    #validates :name, uniqueness: true #activar en fase de producción
    #validates :name, format: { with: /\A[a-zA-Z]+\z/,  #funciona, pero algunos de los paises usados en faker tienen caracteres no validos
    #message: "Solo se permiten letras" }
    
    
end
