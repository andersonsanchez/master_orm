#debes validar que date no sea del futuro
class Travel < ActiveRecord::Base
    belongs_to :routes_shipping_company
    belongs_to :rented_container
    belongs_to :vessel
    belongs_to :bl_master
    #has_one :routes_shipping_company, dependent: :destroy
    has_one :vessel, dependent: :destroy
    validates :board_date, presence: true
    validates :berth_date, presence: true
    validates :code, presence: true
    validates :routes_shipping_company_id, presence: true
    validates :vessel_id, presence: true
    validates :code, length: { in: 12..12}
end