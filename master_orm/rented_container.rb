class RentedContainer < ActiveRecord::Base
    has_many :travels
    belongs_to :container
    validates :travel_id, presence: true
    validates :container_id, presence: true
    
end