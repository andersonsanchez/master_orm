class Consignee < ActiveRecord::Base
    has_many :bl_houses
    belongs_to :bl_house
    validates :id_card_or_rif, presence: true
    #validates :id_card_or_rif, uniqueness: true
    validates :name, presence: true
end