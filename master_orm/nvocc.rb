class Nvocc < ActiveRecord::Base
    
    has_many :routes_nvoccs, dependent: :destroy
    has_many :bl_houses, dependent: :destroy
    has_many :bl_masters, dependent: :destroy
    belongs_to :bl_house
    belongs_to :bl_master
    validates :name, presence: true
    #validates :name, uniqueness: true
    validates :rif, presence: true
    #validates :rif, uniqueness: true
end