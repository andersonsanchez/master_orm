class ShippingCompany < ActiveRecord::Base
    has_many :bl_masters, dependent: :destroy
    has_many :containers, dependent: :destroy
    has_many :vessels, dependent: :destroy
    has_many :routes_shipping_companies, dependent: :destroy
    belongs_to :bl_master
    validates :name, presence: true
    #validates :name, uniqueness: true
    validates :rif, presence: true
    #validates :rif, uniqueness: true

    def self.show_list()
	joins(:bl_masters)
    end
    
end