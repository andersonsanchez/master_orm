#debes validar que date no sea del futuro
class BlMaster< ActiveRecord::Base
    belongs_to :shipping_company
    belongs_to :nvocc
    belongs_to :bl_house
    belongs_to :travel
    has_many :travels
    has_many :nvoccs, dependent: :destroy
    has_many :bl_houses, dependent: :destroy
    has_many :routes_shipping_companies, through: :travels
    validates :date, presence: true
    validates :shipping_company_id, presence: true
    validates :nvocc_id, presence: true
    validates :travel_id, presence: true

    #Representacion de los datos de un bl físico
    #Falta el origen y destino, debo traerlos desde routes_shipping_companies
    def self.show_bl
      system ("clear")
      print "                   ***********************REPRESENTACIÓN DE UN BL-MASTER FÍSICO*********************** \n".colorize(:color => :light_cyan)          
      print "#{joins(:shipping_company, :nvocc, :travel).map(&:data).first}\n"
    end

    def data
      "ID BL-MASTER: #{id} |FECHA: #{date} | NAVIERA: #{shipping_company.name} | NVOCC: #{nvocc.name} |FECHA DE ABORDAJE: #{travel.board_date} |FECHA DE ATRAQUE: #{travel.berth_date} |CODIGO DEL VIAJE: #{travel.code} |ID DEL VIAJE: #{travel.id}".colorize(:color => :light_cyan)
    end
 
               #Un listado todos los BL por naviera
    def self.show_list
      print "                   ***********************LISTADO DE BLS POR NAVIERA*********************** \n".colorize(:color => :light_cyan)
      print " #{joins(:shipping_company).group(:name).order(:id).map(&:lista)}"
    end
    
    def lista
      "ID BL-MASTER: #{id} | NAVIERA: #{shipping_company.name} "
    end
    
end