#en numericality se puede añadir otra restriccion, que solo acepte los numeros 20 ó 40
#por logica de negocios podrias agregar el campo min_capacity
class Vessel < ActiveRecord::Base
    belongs_to :shipping_company
    belongs_to :travel
    has_many :travels, dependent: :destroy
    validates :name, presence: true
    #validates :name, uniqueness: true
    validates :max_capacity, presence: true
    validates :max_capacity, numericality: { only_integer: true }
    validates :max_capacity, length: { in: 0..4 }
    validates :shipping_company_id, presence: true
end