class RoutesShippingCompany < ActiveRecord::Base
    belongs_to :shipping_company
    belongs_to :travel
    has_one :origin
    has_one :destination
    has_many :travels
    has_many :bl_masters, through: :travels
    validates :price_st_container, presence: true
    validates :price_st_container, numericality: true
    validates :price_hq_container, presence: true
    validates :price_hq_container, numericality: true
    validates :timespan, presence: true
    validates :timespan, numericality: true
    validates :shipping_company_id, presence: true
    validates :origin_id, presence: true
    validates :destination_id, presence: true
end