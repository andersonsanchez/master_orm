
class RoutesNvocc < ActiveRecord::Base
    belongs_to :nvocc
    belongs_to :bl_house
    has_one :origin, dependent: :destroy
    has_one :destination, dependent: :destroy
    validates :price_st_container, presence: true
    validates :price_st_container, numericality: true
    validates :price_hq_container, presence: true
    validates :price_hq_container, numericality: true
    validates :timespan, presence: true
    validates :timespan, numericality: true
    validates :nvocc_id, presence: true
    validates :origin_id, presence: true
    validates :destination_id, presence: true
end