class Container < ActiveRecord::Base
    belongs_to :shipping_company
    has_many :rented_containers, dependent: :destroy
    validates :code, presence: true
    validates :code, length: { in: 12..12}
    #validates :code, uniqueness: true
    validates :size, presence: true
    validates :size, numericality: { only_integer: true }
    validates :size, length: { in: 2..2 }
    validates :kind, presence: true
    validates :shipping_company_id, presence: true
end